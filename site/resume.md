# Josh Carton

| <josh.carton@gmail.com> |
|-|
| <https://gitlab.com/druidgreeneyes> |
| <https://www.linkedin.com/in/joshcarton> |

Passionate and versatile software engineer with enterprise application development capabilities in multiple languages and platforms with a strong background in Java and Spring and related JVM languages.  An adaptable teammate with a contagious enthusiasm for learning. Proven ability to come up to speed quickly and apply knowledge and skills to existing tasks or problems to contribute to any project. Adept with multiple programming languages and various automation and Big Data tools. Familiar with standard tools and practices for application monitoring, metrics, and alerting.  

Detailed and proficient problem solver who is quick to analyze, identify, and troubleshoot issues. Applies research, applicable best practices, and tools to create innovative solutions. An excellent communicator and preferred teammate skilled at establishing strong working relationships with stakeholders at all levels within the organization. Seeks challenges and enjoys purposeful work.

## Highlights

- Leads teams forward by offering experience, knowledge, and mentorship to promote growth and collaboration between team members. 

- Develops applications using multiple languages including Java and Kotlin and industry standard frameworks like Spring Boot.  

- Familiar with Microservices architecture practices and techniques using frameworks like Spring Boot and enterprise messaging with tools like Kafka. 

- Comfortable with DevOps build and automation tooling and scripting with tools like Docker, Git, GitHub, GitLab and platforms such as Kubernetes. 

- Adept at font-end development using modern JavaScript frameworks using React, Redux and TypeScript. 

- Proponent of a quality mindset with test automation using tools such as Junit and AssertJ. 

## Experience
### Consultant: [Solution Design Group](https://www.solutiondesign.com/), December 2017-Present

#### Client One (1 year)
- Helped to develop and maintain an MDM application using microservices/cloud-friendly architecture and event-sourcing design patterns. Was involved in many facets of the project, including bugfixing, architectural design, build workflows, test workflows, feature development, and agile/project-management workflows.
- Helped the project migrate from software installs to docker images for local development
- Used Kafka Streams API to drive fluent, readable merging of change events into final state objects
- Used Spring Cloud Streams API to perform stream-based matching of change events to one another
- Used Spring Data, JPA, Hibernate, and raw SQL to optimize ingestion of batch data files into database tables, and then to parallelize change capture and object assembly across those tables
- Used Spock and Spring Test frameworks to write unit, integration, and functional/bdd tests, and to write tools to ease the testing process.
- Wrote Jenkins scripts to manage a monorepo with many separate applications in it.
- Maintained applications written in Java, Kotlin, Groovy, and Scala, using many different APIs to interact with Kafka, including Spring Kafka, Spring Cloud Stream Kafka, Spring Reactor Kafka, Kafka Streams DSL, and the Scala Kafka API.
- Helped the team develop and refine our architecture, working toward simpicity, ease of reasoning, and single-responsibility as core coals for delivering business value.
- Refactored many parts of our application for speed and readability, using functional paradigms to prove logical assertions and reduce chances for error.

Technologies used: Java 8, Spring, Spring Boot, Spring Data, Spring Cloud, Kafka, Groovy, Kotlin, Scala, Spark, Jenkins, Kubernetes, SQLServer

#### Client Two (First Team, 7 months)
- Added and integrated DataDog APM on DotNetCore on Alpine Linux
- Refactored the Drone build pipeline to enable:
    - automated deployments
    - automated promotion up the environment scale
    - automatic release page content
    - automatic image tagging for production and developer use
    - automatic code analysis and blocking
    - automatic takedown and redeployment through GitHub Deployment triggers
- Advocated for cultural improvements and developer happiness via:
    - political support for first-class testing practices
    - asking for what we actually want instead of just what we think we can have
    
Technologies used: C#, Drone, Kubernetes, Bash, Client platform wrapping around AWS tools.

#### Client Two (Second Team, 16 months)
- Built a POC cookiecutter (python/flask) template to enable easier deployment to the client's cloud platform by data science teams
- Advocated (with limited success) for process changes to facilitate communication and continual improvement via:
    - Judgement-Free, Semi-synchronous retros
    - Tightly managed standups (and other meetings)
    - Cross-functional involvement instead of isolating individual contributors on their own work tracks
- Interfaced directly with template users to gather needs, goals, and failures into tickets and improvements
- Built a cli wrapper to enable:
    - Template modularity
    - User-sourced templates
    - Abstraction around various tools (conda, git, cookiecutter) involved in the process of starting a project
    - Updating a project based on updates to that project's template
    - User-sourced extensions to the CLI itself
- Wrote and maintained extensive documentation to promote user happiness and easy adoption.
- Interfaced directly with users on a continuing basis to provide support and gather bugs, documentation failures, and other common problems.
- Learned R in order to refactor data science code for:
    - Parallelism of hot code paths
    - Offloading common tasks onto third-party libraries instead of using hand-rolled implementations
    
Technologies used: Python, Flask, Drone, Kubernetes, Bash, R, Tidyverse, Client platform wrapping around AWS tools.

#### Client Three (8 months)
- Refined type-level encodings of front-end business logic to reduce boilerplate and allow for easier and more effective maintenance.  
- Refined team approach to Kotlin backend patterns for better consistency and easier maintenance.  
- Championed low-investment solutions to various problems, enabling faster turn-around and more flexibility over time.  

Technologies used: Kotlin, Spring Boot, TypeScript, React, Material UI, Docker, and Kubernetes.

#### Client Four (1 year)
- Led the development team with regular office hours, open collaboration, and 1:1 coaching. 
- Used functional and typelevel programming techniques to provide elegant, domain-driven, self-documenting solutions to complex problems with many edge cases.
- Accounted for continual goal and deadline churn by advocating for patterns and practices that would enable piecemeal adaptation instead of requiring wholesale refactors when changes came in. 

Technologies used: Spring Boot, Docker, Kubernetes, Kafka, RabbitMQ, MongoDB, GitHub Workflows

#### Client Five (9+ months ongoing)
- Became the team trailblazer, setting down flexible and maintainable patterns to handle new concerns as they surfaced.
- Championed api and contract-based development techniques, enabling easier on-boarding for downstream applications.
- Championed behavior-driven design and testing patterns, enabling easier communication between developers and non-technical stakeholders on the project.

Technilogies used: Spring Boot, Project Reactor, Spock, Jqwik, Apache Thymeleaf, Kafka, SQLServer, GitHub Workflows

### Software Development/NLP Contractor: Leidos Corporation, June-September 2017
- Built parsing and extraction tools for semi-structured text, looked for algorithmic ways to define and 
extract meaningful features.
- Consulted and worked on Random-Index Vectoring, Apache Spark, and functional code style in Java.

Technologies used: Spark, Java 8

### DevOps Contractor: Retrieval Systems Corporation, May 2016–June 2018
- Built reproducible development vms with Vagrant.
- Designed and Built CI/CD Pipeline through Jenkins and Github, with continued research and 
updates to promote future-proofing, reliability, and ease of use.
- Migrated aging Java/Web scripts and software application to AWS infrastructure. 
- Scripting multiple tomcat instance deployments on Amazon Linux AMIs.
- Troubleshot deployment issues, developed software and library upgrade recommendations, 
set up and configured Apache/Tomcat stack, worked through application bugs, developed new 
AWS architecture.

Technologies used: Vagrant, Jenkins, Github, AWS, Java, Bash, Apache, Tomcat, CentOS, Ubuntu

### Software Development Contractor: Aitheras LLC, August 2016–December 2016
- Researched and implemented Random-Index Vector library in Java 8. Placed in Open 
Source on Github. Optimized for fastest implementation characteristics.
- Researched and implemented LDA variant in Java using Random Index Vectors to produce document 
clusters and topic labels.
- Provided tech support and training on RIV, Spark, Java 8 Lambda syntax, and functional programming.
- Built a text-analysis engine on AWS using Spark, HDFS,
Zookeeper and HBase. 

Technologies used: Java 8, Python, Scala, Github, Spark, Hadoop, HDFS, Hbase

### Event Services Coordinator: Sonic Foundry Inc., June 2012–August 2015
- Built a command-line interface in C# to client software.
- Used cli to automate software testing and configuration, increased manual process throughput by 
50%.
- Prototyped a low-maintenance version in F#.

Technologies used: C#, F#, Batch, Windows

### Tech Support II: Beloit College, August 2010–May 2012
- Provided AV and tech support for the Beloit College campus. 
- Built a customized distribution of Fedora to run on aging hardware in the school’s computer labs. Used bash-scripted timed-inactivity lockouts and user-folder overwrites to imitate the effects of DeepFreeze.

Technologies Used: Linux, Bash, Windows
